Linode API Wrapper (forked)
==================

A Linode API wrapper using Guzzle and implemented using simple Command objects

By [Simon Hampel](http://hampelgroup.com/).

Thanks to [Everett Griffiths](http://fireproofsocks.com/) for implementing additional API calls.

Installation
------------

The recommended way of installing Hampel Linode is through [Composer](http://getcomposer.org):

    :::json
    {
        "require": {
            "fwartner/linode": "~3.1"
        }
    }

Note that if you intend to use this package with Laravel, we recommend installing the
[hampel/linode-laravel](https://packagist.org/packages/hampel/linode-laravel) package instead, which provides a simple
Laravel service provider and Facade for working with this API wrapper.

Usage
-----

*Note: v3.0 of this wrapper is not backwards compatible with v1.0 or v2.0 versions - applications using this wrapper
will need to be re-written to utilise this new interface.*

    :::php
    <?php

    use Hampel\Linode\Linode;
    use Hampel\Linode\Commands\TestCommand;
    use Hampel\Linode\Commands\UserCommand;
    use Hampel\Linode\Commands\DomainCommand;

    // long-hand initialisation method
    $client = new GuzzleHttp\Client();
    $linode = new Linode($client, 'your api key here');

    // alternative static factory
    $linode = Linode::make('your api key here');

    // create a command object for the Linode service to execute
    // in this case, a simple echo test, API echos back parameters passed
    $command = new TestCommand('echo', ['foo' => 'bar']);

    // pass the command to Linode service
    $response = $linode->execute($command);

    var_dump($response);
    
    // get api key
    $command = new UserCommand('getapikey', ['username' => 'foo', 'password' => 'bar']);
    $response = $linode->execute($command);

    var_dump($response);

    // create a new domain entry
    $options = array("soa_email" => "soa_email@domain-name.com", "ttl_sec" => 3600);
    $command = new DomainCommand('create', $options);
    $response = $linode->execute($command);

    var_dump($response);
    
    // retrieve details about a domain
    $response = $linode->execute(new DomainCommand('list', ['domainid' => 12345])); // specify domain id as parameter
    
    var_dump($response);
    ?>

Example: Creating a New Linode
------------------------------

Sometimes it's not clear how the individual API commands fit together to bring up a new server.
The process usually goes something like this:

* Create the Linode (this acts as a container)
* Create the primary disk (e.g. from a stack script)
* Create the swap disk
* Create the configuration profile
* Boot the Linode

Below is the outline in code.  For production use, consider wrapping the command execution in try/catch blocks.

    :::php
    <?php

    use Hampel\Linode\Linode;
    use Hampel\Linode\Commands\LinodeCommand;
    use Hampel\Linode\Commands\LinodeDiskCommand;
    use Hampel\Linode\Commands\LinodeConfigCommand;

    $linode = Linode::make('your api key here');

    // Create the Linode
    $response = $linode->execute(new LinodeCommand('create', [
        'datacenterid' => 2, // from avail.datacenters()
        'planid' => 2, // from avail.LinodePlans()
        'paymentterm' => 1
    ]));

    $linodeid = $response['LinodeID'];

    // Create the Primary Disk
    $response = $linode->execute(new LinodeDiskCommand('createfromstackscript', [
        'linodeid' => $linodeid, // the one we just created
        'stackscriptid' => 123,
        'stackscriptudfresponses' => '{}',
        'distributionid' => 89,
        'label' => 'PrimaryDisk',
        'size' => 24576
        'rootpass' => 'xxxxxxxx'
    ]));
    $primarydiskid = $response['DiskID'];

    // Create the Swap Disk
    $response = $linode->execute(new LinodeDiskCommand('create', [
        'linodeid' => $linodeid,
        'label' => 'SwapDisk',
        'type' => 'swap',
        'size' => 256
    ]));
    $swapdiskid = $response['DiskID'];

    // Create the Configuration Profile
    $response = $linode->execute(new LinodeConfigCommand('create', [
        'linodeid' => $linodeid,
        'kernelid' => 138, // from avail.kernels()
        'label' => 'StandardProfile',
        'disklist' => "$primarydiskid,$swapdiskid,,,,,,,",
        'rootdevicenum' => 1
    ]));
    $configid = $response['ConfigID'];

    // Boot the Linode
    $response = $linode->execute(new LinodeCommand('boot', [
        'linodeid' => $linodeid,
        'configid' => $configid
    ]));

    ?>


Notes
-----
 
The following calls have been implemented:

* account.*
* api.*
* avail.*
* domain.*
* domain.resource.*
* linode.*
* linode.config.*
* linode.disk.*
* linode.ip.*
* linode.job.*
* nodebalancer.*
* nodebalancer.config.*
* nodebalancer.node.*
* stackscript.*
* test.*
* user.*


TODO:
-----

Write network tests for all commands (this may incur credit card charges for some tests!)


Unit Testing
------------

Rename phpunit.xml.dist to phpunit.xml to set up unit testing, configure your API Key in the php section:

    :::xml
    <php>
        <const name="API_KEY" value="Linode API Key goes here" />
    </php>

To run mock tests only and ignore network tests, run: `phpunit --exclude-group network`

There are some additional tests which will incur credit card charges on your Linode account and as such, are disabled by
default. You can run these specific tests by running the command: `phpunit --group chargeable`

**WARNING:**

Running the chargeable group tests will incur charges on your credit card!  Even though servers created by
the unit tests are deleted in the cleanup process, the amount refunded will not match the amount billed!
If a unit test fails, the server(s) will remain in your Linode account and you will be charged for them:
in this case you must delete them manually.