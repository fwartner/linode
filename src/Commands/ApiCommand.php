<?php namespace Hampel\Linode\Commands;

class ApiCommand extends Command
{
    /** @var string the command prefix */
    protected $prefix = 'api';

    /** @var array allowable actions for $action parameter */
    protected $allowed_actions = ['spec'];

    /** @var array allowable parameters to create and update calls */
    protected $allowed_parameters = [];
}
