<?php namespace Hampel\Linode\Commands;

class LinodeDiskCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'linode.disk';

    /** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['create', 'createfromdistribution', 'createfromimage', 'createfromstackscript', 'delete', 'duplicate', 'imagize', 'list', 'resize', 'update'];
	
	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
		'linodeid', // required - numeric LinodeID
		'distributionid', // optional - numeric
        'stackscriptid', // optional - numeric
        'stackscriptudfresponses', // optional - string json formatted
		'rootpass',  // optional - string for root password
		'rootsshkey', // optional - string SSH key
		'label', // required - string The display label for this Disk
		'type', // required - string The formatted type of this disk. Valid types are: ext3, ext4, swap, raw
		'isreadonly', // optional - boolean Enable forced read-only for this Disk
		'size', // required - numeric The size in MB of this Disk.
	];
}
