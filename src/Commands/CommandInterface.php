<?php namespace Hampel\Linode\Commands;

interface CommandInterface
{
	/**
	 * @return string 	the name of the action for this command
	 */
	public function getAction();

	/**
	 * @param string $api_key	api key to include in the command
	 *
	 * @return array    		the array of key-value pairs to sent for the command
	 */
	public function build($api_key = '');
}
