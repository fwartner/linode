<?php namespace Hampel\Linode\Commands;

class AccountCommand extends Command
{
    /** @var string the command prefix */
    protected $prefix = 'account';

    /** @var array allowable actions for $action parameter */
    protected $allowed_actions = ['estimateinvoice', 'info'];

    /** @var array allowable parameters to create and update calls */
    protected $allowed_parameters = [
        'mode', // required - string This is one of the following options: 'linode_new', 'linode_resize', or 'nodebalancer_new'
        'paymentterm', // optional - numeric Subscription term in months. One of: 1, 12, or 24. This is required for modes 'linode_new' and 'nodebalancer_new'.
        'planid',  // optional - numeric The desired PlanID available from avail.LinodePlans(). This is required for modes 'linode_new' and 'linode_resize'.
        'linodeid' // optional - numeric This is the LinodeID you want to resize and is required for mode 'linode_resize'.
    ];
}
