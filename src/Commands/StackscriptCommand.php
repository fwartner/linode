<?php namespace Hampel\Linode\Commands;

class StackscriptCommand extends Command
{
    /** @var string the command prefix */
    protected $prefix = 'stackscript';

    /** @var array allowable actions for $action parameter */
    protected $allowed_actions = ['create', 'delete', 'list', 'update'];

    /** @var array allowable parameters to create and update calls */
    protected $allowed_parameters = [
        'label', // required
        'description', // option - string
        'distributionidlist', // required - Comma delimited list of DistributionIDs that this script works on
        'ispublic', // optional - boolean
        'rev_note', // optional - string
        'script' // required - string, the actual script
    ];
}
