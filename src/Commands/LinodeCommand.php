<?php namespace Hampel\Linode\Commands;
/**
 * Class LinodeCommand
 * This is what handles creation of Linodes and you will get charged for creating them!
 * There is a 75-linodes-per-hour limiter.
 * @package Hampel\Linode\Commands
 */
class LinodeCommand extends Command
{
	/** @var string the command prefix */
	protected $prefix = 'linode';

    /** @var array allowable actions for $action parameter */
	protected $allowed_actions = ['boot', 'clone', 'create', 'delete', 'list', 'reboot', 'resize', 'shutdown', 'update'];
	
	/** @var array allowable parameters to create and update calls */
	protected $allowed_parameters = [
	    'linodeid', // numeric
        'configid', // numeric
		'datacenterid', // required - The DatacenterID from avail.datacenters() where you wish to place this new Linode
		'planid', // required - The desired PlanID available from avail.LinodePlans()
		'paymentterm', // optional - One of: 1, 12, or 24
	];
}
