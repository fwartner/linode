<?php namespace Hampel\Linode\Exception;

use GuzzleHttp\Message\Response;

class LinodeException extends \Exception
{
	protected $response;

	public function __construct($message = "", $code = 0, \Exception $previous = null, Response $response = null)
	{
		if (! is_null($response))
		{
			$this->response = $response;
		}
		else
		{
			if (! is_null($previous) AND ($previous instanceof RequestException))
			{
				if ($previous->hasResponse())
				{
					$this->response = $previous->getResponse();
				}
			}
		}

		parent::__construct($message, $code, $previous);
	}

	public function getResponse()
	{
		return $this->response;
	}
}
