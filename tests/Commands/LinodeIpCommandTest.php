<?php namespace Hampel\Linode\Commands;

use Mockery;
use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class LinodeIpCommandTest extends \PHPUnit_Framework_TestCase
{
    protected $linode;
    protected $client;
    protected $command;

    public function setUp()
    {
        date_default_timezone_set('UTC');

        $this->mock = new Mock();

        $this->client = new Client();
        $this->client->getEmitter()->attach($this->mock);
    }

    /**
     * Where we store sample JSON responses.
     * @return string
     */
    protected function getMockPath()
    {
        return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
    }

    /**
     * Mock...
     */
    public function testMockAddPrivate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.ip.addprivate');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.ip.addprivate',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_ip_addprivate.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.ip.addprivate&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('IPAddressID', $response);
        $this->assertEquals('8364', $response['IPAddressID']);

    }

    /**
     * Mock...
     */
    public function testMockAddPublic()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.ip.addpublic');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.ip.addpublic',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_ip_addpublic.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.ip.addpublic&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('IPAddressID', $response);
        $this->assertEquals('5384', $response['IPAddressID']);

    }


    /**
     *
     */
    public function testMockList()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.ip.list');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.ip.list',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_ip_list.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.ip.list&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertTrue(is_array($response));
    }

    /**
     *
     */
    public function testMockSetRDNS()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.ip.setrdns');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.ip.setrdns',
            'ipaddress' => '123.123.123.123',
            'hostname' => 'li13-10.members.linode.com'
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_ip_setrdns.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.ip.setrdns&ipaddress=123.123.123.123&hostname=li13-10.members.linode.com', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('HOSTNAME', $response);
        $this->assertEquals('li13-10.members.linode.com', $response['HOSTNAME']);
    }

    /**
     *
     */
    public function testMockSwap()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.ip.swap');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.ip.swap',
            'ipaddressid' => 123,
            'withipaddressid' => 456
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_ip_swap.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.ip.swap&ipaddressid=123&withipaddressid=456', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertTrue(is_array($response));
    }

    /**
     *
     */
    public function tearDown()
    {
        Mockery::close();
    }
}
