<?php namespace Hampel\Linode\Commands;

use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class StackscriptCommandTest extends \PHPUnit_Framework_TestCase
{

    protected $linode;

    public function setUp()
    {
        date_default_timezone_set('UTC');
        $this->linode = Linode::make(API_KEY);
    }

    /**
     * @group network
     */
    public function testList()
    {
        $response = $this->linode->execute(new StackscriptCommand('list', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
    }
}
