<?php namespace Hampel\Linode\Commands;

use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class TestCommandTest extends \PHPUnit_Framework_TestCase
{
	protected $linode;

	public function setUp()
	{
		date_default_timezone_set('UTC');

		$this->mock = new Mock();

		$this->client = new Client();
		$this->client->getEmitter()->attach($this->mock);

		$this->linode = new Linode($this->client);
	}

	protected function getMockPath()
	{
		return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
	}

	/**
	 * @group network
	 */
	public function testEcho()
	{
		$linode = Linode::make(API_KEY);

		$response = $linode->execute(new TestCommand('echo', ['foo' => 'bar']));

		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertArrayHasKey('foo', $response);
		$this->assertEquals('bar', $response['foo']);
	}

	public function testMockEcho()
	{
		$this->mock->addResponse($this->getMockPath() . 'test_echo.json');

		$response = $this->linode->execute(new TestCommand('echo', ['foo' => 'bar']));

		$this->assertEquals(200, $this->linode->getLastStatusCode());
		$this->assertArrayHasKey('foo', $response);
		$this->assertEquals('bar', $response['foo']);
	}
}
