<?php namespace Hampel\Linode\Commands;

use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class AvailCommandTest extends \PHPUnit_Framework_TestCase
{

    protected $linode;

    public function setUp()
    {
        date_default_timezone_set('UTC');
        $this->linode = Linode::make(API_KEY);
    }

    /**
     * @group network
     */
    public function testDatacenters()
    {
        $response = $this->linode->execute(new AvailCommand('datacenters', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertGreaterThanOrEqual(6, count($response)); // this will change if they add new datacenters
    }

    /**
     * @group network
     */
    public function testDistributions()
    {
        $response = $this->linode->execute(new AvailCommand('distributions', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertTrue(is_array($response));

        $this->assertGreaterThanOrEqual(20, count($response)); // kinda dumb

        $response = $this->linode->execute(new AvailCommand('distributions', ['distributionid' => 127]));
        $this->assertEquals(200, $this->linode->getLastStatusCode());

        $this->assertTrue(is_array($response));
        $response = array_shift($response);
        $this->assertArrayHasKey('REQUIRESPVOPSKERNEL', $response);
        $this->assertArrayHasKey('LABEL', $response);
        $this->assertArrayHasKey('IS64BIT',$response);

        $this->assertEquals($response['LABEL'], 'CentOS 6.5');
    }

    /**
     * @group network
     */
    public function testKernels()
    {
        $response = $this->linode->execute(new AvailCommand('kernels', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertGreaterThanOrEqual(3, count($response)); // this will change if they add more...
    }

    /**
     * @group network
     */
    public function testLinodeplans()
    {
        $response = $this->linode->execute(new AvailCommand('linodeplans', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
        $this->assertGreaterThanOrEqual(3, count($response));
    }

    /**
     * @group network
     */
    public function testNodebalancers()
    {
        $response = $this->linode->execute(new AvailCommand('nodebalancers', []));
        $this->assertEquals(200, $this->linode->getLastStatusCode());
    }

    /**
	 * TODO: work out why this is giving me "json_decode(): integer overflow detected" errors
     * @group network
     */
//    public function testStackscripts()
//    {
//        $response = $this->linode->execute(new AvailCommand('stackscripts', ['distributionidlist'=>'124']));
//        $this->assertEquals(200, $this->linode->getLastStatusCode());
//        $this->assertGreaterThanOrEqual(708, count($response));
//    }
}
