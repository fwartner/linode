<?php namespace Hampel\Linode\Commands;

use Mockery;
use GuzzleHttp\Client;
use Hampel\Linode\Linode;
use GuzzleHttp\Subscriber\Mock;

class LinodeConfigCommandTest extends \PHPUnit_Framework_TestCase
{

    protected $linode;
    protected $client;
    protected $command;

    public function setUp()
    {
        date_default_timezone_set('UTC');

        $this->mock = new Mock();

        $this->client = new Client();
        $this->client->getEmitter()->attach($this->mock);

    }

    /**
     * Where we store sample JSON responses.
     * @return string
     */
    protected function getMockPath()
    {
        return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
    }

    /**
     * Mock...
     */
    public function testMockCreate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.config.create');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.config.create',
            'linodeid' => 123,
            'kernelid' => 2,
            'label' => 'Test'
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_config_create.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.config.create&linodeid=123&kernelid=2&label=Test', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('ConfigID', $response);
        $this->assertEquals('31239', $response['ConfigID']);

    }

    /**
     *
     */
    public function testMockDelete()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.config.delete');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.config.delete',
            'linodeid' => 123,
            'configid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_config_delete.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.config.delete&linodeid=123&configid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('ConfigID', $response);
        $this->assertEquals('31239', $response['ConfigID']);

    }

    /**
     *
     */
    public function testMockList()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.config.list');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.config.list',
            'linodeid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_config_list.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.config.list&linodeid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertTrue(is_array($response));
    }


    /**
     *
     */
    public function testMockUpdate()
    {
        $command = Mockery::mock('Hampel\Linode\Commands\CommandInterface');
        $command->shouldReceive('getAction')->andReturn('linode.config.update');
        $command->shouldReceive('build')->andReturn([
            'api_action' => 'linode.config.update',
            'linodeid' => 123,
            'configid' => 123
        ]);

        $this->mock->addResponse($this->getMockPath() . 'linode_config_update.json');

        $linode = new Linode($this->client);

        $response = $linode->execute($command);

        $this->assertInstanceOf('GuzzleHttp\Message\Response', $linode->getLastResponse());
        $this->assertEquals('?api_action=linode.config.update&linodeid=123&configid=123', $linode->getLastQuery());
        $this->assertEquals(200, $linode->getLastStatusCode());
        $this->assertArrayHasKey('ConfigID', $response);
        $this->assertEquals('31239', $response['ConfigID']);
    }

    /**
     *
     */
    public function tearDown()
    {
        Mockery::close();
    }
}
